#!/usr/bin/env bash
echo "yes" | python manage.py collectstatic
gunicorn -w 3 --chdir ./ fasad.wsgi --bind 0.0.0.0:8000
