YaMapsShown = false;

$(document).ready(function(){

  $('.navigation__button').click(function() {
    $('.hero__nav').toggleClass('open-menu');
  });

  $('.system__category-head').click(function() {
    $('.system__category').toggleClass('category-open');
    $('.system__category-wrapper').toggleClass('category-wrapper-open')
  });

  //E-mail Ajax Send
	$("form").submit(function() { //Change
    var th = $(this);
		$.ajax({
        type: "POST",
        url: "/request", //Change
        data: th.serializeArray(),
		}).done(function() {
      th.trigger("reset");
      $.magnificPopup.close();
      window.location.href = "/thanks";
			setTimeout(function() {
        // Done Functions
        $.magnificPopup.close();
			}, 3000);
		});
		return false;
  });

  $("a.scroll-link").mPageScroll2id();

  $('.call-bt').magnificPopup({
    items: {
      src: '#call-popups',
    }
  });

  $('.system__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    prevArrow: $('.system__slide-prev'),
    nextArrow: $('.system__slide-next'),
    infinite: false,
    adaptiveHeight: true
  });

  $('.system__slider').on('afterChange', function(event, slick, currentSlide){
    let currentLabel = $('.system__category-item').eq(currentSlide);
    $('.system__category-item').removeClass('active');
    $(currentLabel).addClass('active');
  });

  $(".system__category .system__category-item").click(function(e){
    $('.system__category-item').removeClass('active');
    $('.system__category').removeClass('category-open');
    $('.system__category-wrapper').removeClass('category-wrapper-open');
    $(this).addClass('active');
    let slideIndex = $(this).index();
    $('.system__slider').slick('slickGoTo', parseInt(slideIndex));
  });

  $('.projection__block-slider').each(function(key, item) {

    var sliderIdName = 'slider' + key;
    var sliderNavIdName = 'slider-dots' + key;

    this.id = sliderIdName;
    $('.projection__block-slider-dots')[key].id = sliderNavIdName;

    var sliderNavId = '#' + sliderNavIdName;
    var sliderId = '#' + sliderIdName;

    $(sliderId).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable:true,
      infinite: false,
      speed: 100,
      dots: true,
      appendDots: sliderNavId,
      prevArrow: false,
      pauseOnHover: false,
      nextArrow: false,
    });

    if($(sliderId).find('.slick-list')) {
      $(this).mouseenter(function(e) {
        $('<div class="slidecount-wrapper"></div>').appendTo(sliderId);
        let slideCount = $(sliderId).slick("getSlick").slideCount;
        for (let i = 0; i < slideCount; i++) {
          $(this).find('.slidecount-wrapper').append('<div class="slidecount"></div>');
        }
      });
      $(this).mouseleave(function(e) {
        $('.slidecount-wrapper').remove();
      });
    }

    if($(sliderId).find('.slidecount')) {
      $(this).mouseenter(function(e) {
        $('.slidecount').mouseenter(function(e) {
          let slideIndex = $(this).index();
          $(sliderId).slick('slickGoTo', parseInt(slideIndex));
        });
      });
    }

  });

  $('.footer__navigation--btn').click(function() {
    $('.footer__navigation').toggleClass('footer__navigation--is-opened');
  });

  $(window).scroll(function() {
    if (!YaMapsShown){
     if($(window).scrollTop() + $(window).height() > $(document).height() - 700) {
      showYaMaps();
      YaMapsShown = true;
     }
    }
  });

});

function showYaMaps(){
  var iframe   = document.createElement("iframe");
  iframe.src   = "https://yandex.ru/map-widget/v1/?um=constructor%3Abde451488702101f16f1b6cf16c8d90c286a4e84d2b9917f9012e6cf89021cc5&amp;source=constructor";
  iframe.width = "100%";
  iframe.height = "100%";
  document.getElementById("YaMaps").appendChild(iframe);
 }